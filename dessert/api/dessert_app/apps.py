from django.apps import AppConfig


class DessertAppConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "dessert_app"
