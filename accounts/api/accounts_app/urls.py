from django.urls import path
from .views import (
    list_accounts,
    user_detail
)

urlpatterns = [
    path("accounts/", list_accounts, name="list_accounts"),
    path("accounts/<int:id>/", user_detail, name="user_detail")
]
