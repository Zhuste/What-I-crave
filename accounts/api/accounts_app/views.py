from django.shortcuts import render, redirect
from django.db import IntegrityError
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
from .models import User
from ....common.json import ModelEncoder
import json


class AccountModelEncoder(ModelEncoder):
    model = User
    properties = [
        "first_name",
        "middle_name",
        "last_name",
        "email",
        "username",
        "password"
    ]


@require_http_methods(["GET", "POST"])
def list_accounts(request):
    if request.method == "GET":
        users = User.objects.all()
        return JsonResponse(
            {"users": users},
            encoder = AccountModelEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        required_properties = [
            "first_name",
            "last_name",
            "email",
            "username",
            "password",
        ]

        missing_info = []
        for required_property in required_properties:
            if (
                required_property not in content
                or len(content[required_property]) == 0
            ):
                missing_info.append(required_property)
            if missing_info:
                response = {
                    "message": "Missing i1nformation",
                    "properties": missing_info,
                }
                return JsonResponse(
                    response,
                    status=400
                )
        try:
            account = User.objects.create_user(
                firstname=content["first_name"],
                lastname=content["last_name"],
                email=content["email"],
                username=content["username"],
                password=content["password"]
            )
            return JsonResponse(
                {"account": account,
                "message": "Account successfully created"},
                status=200
            )
        except ValueError:
            return JsonResponse(
                {"message": "Invalid data input"},
                status=400
            )
    else:
        return JsonResponse(
            {"message": "No accounts found"},
            status=404
    )

@require_http_methods(["GET", "PUT", "DELETE"])
def user_detail(request, id):
    if request.method == "GET":
        try:
            user = User.objects.get(id=id)
            return JsonResponse(
                user,
                encoder=AccountModelEncoder,
                safe=False,
            )
        except User.DoesNotExist:
            return JsonResponse(
                {"message": "User does not exist"},
                status=404
            )
    elif request.method == "DELETE":
        try:
            user = User.objects.get(id=id)
            user.delete()
            return JsonResponse(
                {"message": "User successfully deleted"},
                status=200
            )
        except User.DoesNotExist:
            return JsonResponse(
                {"message": "User does not exist"},
                status=404
            )
    elif request.method == "PUT":
        try:
            user = User.objects.get(id=id)
        except User.DoesNotExist:
            return JsonResponse(
                {"message": "User not found"},
                status=404
            )

        content = json.loads(request.body)
        if "email" in content:
            del content["email"]
        if "username" in content:
            del content["username"]

        for property in content:
            if property != "password" and hasattr(user, property):
                setattr(user, property, content[property])
            elif property == "password":
                user.set_password(content["password"])

        user.save()
        return JsonResponse(
            user,
            encoder=AccountModelEncoder,
            safe=False,
        )
