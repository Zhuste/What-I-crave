from django.shortcuts import render
import json
from ....common.json import ModelEncoder
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from .models import Recipe, Ingredients, RecipeSteps


class RecipeEncoder(ModelEncoder):
    model = Recipe
    properties = [
        "title",
        "picture",
        "protein",
        "description",
        "servings",
        "cooking_time",
        "author",
        "ingredient",
        "steps"
    ]
class RecipeThumbEncoder(ModelEncoder):
    model = Recipe
    properties = [
        "title",
        "picture"
    ]

class RecipeStepsEncoder(ModelEncoder):
    model = RecipeSteps
    properties = [
        "step_number",
        "instruction",
    ]

class IngredientsEncoder(ModelEncoder):
    model = Ingredients
    properties = [
        "item_name",
        "amount"
    ]


@login_required
@require_http_methods(["GET", "POST"])
def list_recipes(request):
    if request.method == "GET":
        try:
            recipes_list = Recipe.objects.filter(author=request.user)
            return JsonResponse(
                {"recipes": recipes_list},
                encoder=RecipeThumbEncoder,
                safe=False
            )
        except Recipe.DoesNotExist:
            return JsonResponse (
                {"message": "Invalid request or No recipes found"},
                status=404
            )
    elif request.method == "POST":
        recipe_content = json.loads(request.body)
        try:
            new_recipe = Recipe.objects.create(**recipe_content)
            return JsonResponse(
                {"Recipe": new_recipe},
                encoder=RecipeEncoder
            )
        except ValueError:
            return JsonResponse(
                {"message": "Invalid inputs, try again"},
                status=400
            )



@login_required
@require_http_methods(["GET", "PUT", "DELETE"])
def recipe_detail(request, id):
    if request.method == "GET":
        try:
            recipe = Recipe.objects.get(id=id)
            return JsonResponse(
                recipe,
                encoder=RecipeEncoder,
                safe=False,
            )
        except Recipe.DoesNotExist:
            return JsonResponse(
                {"error": "Recipe not found"},
                status=404
            )
    elif request.method == "DELETE":
        try:
            recipe_del = Recipe.objects.get(id=id)
            recipe_del.delete()
            return JsonResponse(
                {"message": "Recipe successfully deleted"},
                status=200
            )
        except Recipe.DoesNotExist:
            return JsonResponse(
            {"message": "Recipe does not exist, try again"},
            status=404
        )
    elif request.method == "PUT":
        recipe_content = json.loads(request.body)
        try:
            recipe_update = Recipe.objects.get(id=id)
            recipe_update.update(**recipe_content)
            return JsonResponse(
                recipe_content,
                encoder=RecipeEncoder,
                safe=False
            )
        except Recipe.DoesNotExist:
            return JsonResponse(
                {"message": "Recipe not found"},
                status=404
            )

@login_required
@require_http_methods(["GET", "POST"])
def list_steps(request):
    if request.method == "GET":
        try:
            steps_listing = RecipeSteps.objects.all()
            return JsonResponse(
                {"recipe steps": steps_listing},
                encoder=RecipeStepsEncoder,
                safe=False
            )
        except RecipeSteps.DoesNotExist:
            return JsonResponse(
                {"message": "Recipe steps not found or there are no steps registered"},
                status=404
            )
    elif request.method == "POST":
        step_content = json.loads(request.body)
        try:
            adding_steps = RecipeSteps.objects.create(**step_content)
            return JsonResponse(
                {"recipe steps": adding_steps},
                encoder=RecipeStepsEncoder
            )
        except RecipeSteps.ValueError:
            return JsonResponse(
                {"message": "Invalid input, try again"},
                status=400
            )

@login_required
@require_http_methods(["GET", "PUT", "DELETE"])
def steps_detail(request, id)
    if request.method == "GET":
        try:
            recipe_steps = RecipeSteps.objects.get(id=id)
            return JsonResponse(
                RecipeSteps,
                encoder=RecipeStepsEncoder,
                safe=False
            )
        except RecipeSteps.DoesNotExist:
            return JsonResponse(
                {"message": "Steps not found"},
                status=404
                )
    elif request.method == "DELETE":
        try:
            steps_del = RecipeSteps.objects.get(id=id)
            steps_del.delete()
            return JsonResponse(
                {"message": "Recipe steps successfully deleted"},
                status=200
            )
        except RecipeSteps.DoesNotExist:
            return JsonResponse(
                {"message": "Recipe steps do not exist, try again"},
                status=404
            )
    elif request.method == "PUT":
        steps_content = json.loads(request.body)
        try:
            steps_update = RecipeSteps.objects.get(id=id)
            steps_update.update(**steps_content)
            return JsonResponse(
                steps_content,
                encoder=RecipeStepsEncoder,
                safe=False
            )
        except RecipeSteps.DoesNotExist:
            return JsonResponse(
                {"message": "Recipe steps not found or invalid id"},
                status=404
            )


@login_required
@require_http_methods(["GET", "POST"])
def list_ingredients(request):
    if request.method == "GET":
        try:
            ingredients_listing = Ingredients.objects.all()
            return JsonResponse(
                {"ingredients": ingredients_listing},
                encoder=IngredientsEncoder,
                safe=False
            )
        except Ingredients.DoesNotExist:
            return JsonResponse(
                {"message": "There are not ingredients listed"},
                status=404
            )
    elif request.method == "POST":
        ingredient_content = json.loads(request.body)
        try:
            adding_ingredient = json.objects.create(**ingredient_content)
            return JsonResponse(
                {"ingredients": adding_ingredient},
                encoder=IngredientsEncoder
            )
        except Ingredients.ValueError:
            return JsonResponse(
                {"message": "Invalid input, try again"},
                status=400
            )

@login_required
@require_http_methods(["GET", "PUT", "DELETE"])
def ingredients_detail(request, id):
    if request.method == "GET":
        try:
            ingredient_items = Ingredients.objects.get(id=id)
            return JsonResponse(
                ingredient_items,
                encoder=IngredientsEncoder,
                safe=False
            )
        except Ingredients.DoesNotExist:
            return JsonResponse(
                {"message": "Ingredient items not found"},
                status=404
            )
    elif request.method == "DELETE":
        try:
            ingredients_del = Ingredients.objects.get(id=id)
            ingredients_del.delete()
            return JsonResponse(
                {"message": "Ingredient items successfully deleted"},
                status=200
            )
        except Ingredients.DoesNotExist:
            return JsonResponse(
                {"message": "Ingredient items not found, check again"},
                status=404
            )
    elif request.method == "PUT":
        ingredients_content = json.loads(request.body)
        try:
            ingredients_update = Ingredients.objects.get(id=id)
            ingredients_update.update(**ingredients_content)
            return JsonResponse(
                ingredients_content,
                encoder=IngredientsEncoder,
                safe=False
            )
        except Ingredients.DoesNotExist:
            return JsonResponse(
                {"message": "Ingredients items not found or invalid id"},
                status=404
            )
