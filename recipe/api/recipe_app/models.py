from django.db import models
from django.urls import reverse
from django.conf import settings


class RecipeSteps(models.Model):
    step_number = models.PositiveSmallIntegerField(null=True, blank=True)
    instruction = models.TextField()

    class Meta:
        ordering = ['step_number']


class Ingredients(models.Model):
    item_name = models.CharField(max_length=200)
    amount = models.CharField(max_length=200)


    class Meta:
        ordering = ['item_name']

class Recipe(models.Model):
    title = models.CharField(max_length=150, unique=True)
    picture = models.URLField()
    protein = models.CharField(max_length=20)
    description = models.TextField()
    servings = models.CharField(max_length=10, null=True, blank=True)
    serving_type = models.CharField(max_length=10, default='Either')
    cooking_time = models.CharField(max_length=30)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True
    )
    ingredient = models.ForeignKey(
        Ingredients,
        related_name='ingredients',
        on_delete=models.CASCADE
    )
    steps = models.ForeignKey(
        RecipeSteps,
        related_name="steps",
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title

    def hot(self):
        self.serving_type = 'Hot'
        self.save()

    def cold(self):
        self.serving_type = 'Cold'
        self.save()

    def get_api_url(self):
        return reverse('api_recipe', kwargs={'id': self.id})
