def list_recipes(request):
    if request.method == "GET":
        recipes_list = Recipe.objects.filter(author=request.user)
        return JsonResponse(
            {"recipes": recipes_list},
            encoder=RecipeThumbEncoder,
            safe=False,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            recipe_data = {
                "title": content["title"],
                "servings": content["servings"],
                "cooking_time": content["cooking_time"],
                "author": request.user,
                "food_type": content["food_type"],
                "protein_type": content["protein_type"]
            }
            new_recipe = Recipe.objects.create(**recipe_data)

            new_recipe_steps_list = []
            new_ingredients_list = []

            for step_data in content["steps"]:
                step = RecipeSteps.objects.create(
                    recipe=new_recipe,
                    step_number=step_data["step_number"],
                    instruction=step_data["instruction"]
                )
                new_recipe_steps_list.append(step)

            for ingredient_data in content["ingredients"]:
                ingredient = Ingredient.objects.create(
                    recipe=new_recipe,
                    item_name=ingredient_data["item_name"],
                    amount=ingredient_data["amount"]
                )
                new_ingredients_list.append(ingredient)


            return JsonResponse(
                {"Recipe": new_recipe},
                encoder=RecipeEncoder
            )
        except:
            return JsonResponse(
                {"message": "Invalid inputs, please check inputs"},
                status=400
            )


data = {
        "id": recipe.id,
        "title": recipe.title,
        "servings": recipe.servings,
        "serving_type": recipe.serving_type,
        "cooking_time": recipe.cooking_time,
        "author": recipe.author.id if recipe.author else None,
        "food_type": recipe.food_type.id,
        "protein_type": recipe.protein_type.id,
        "steps": list(recipe.steps.values('step_number', 'instruction')),
        "ingredients": list(recipe.ingredients.values('amount', 'item_name')),
    }


# example of using auth token
def list_recipes(request):
    # getting the auth token from headers request
    auth_token = request.headers.get("Authorization")
    # using the environment variable from the docker-compose
    accounts_url = os.environ.get("ACCOUNTS_API_URL")
    # making the request to accounts microservice for auth
    headers = {"Authorization": auth_token}

    response = requests.get(accounts_url, headers=headers)


#PUT DELETE
def recipe_detail(request, id):
    if request.method == "GET":
        try:
            recipe = Recipe.objects.get(id=id)
            return JsonResponse(
                recipe,
                encoder=RecipeEncoder,
                safe=False,
            )
        except Recipe.DoesNotExist:
            return JsonResponse(
                {"message": "Recipe not found"},
                status=404
            )
    elif request.method == "DELETE":
        try:
            recipe_del = Recipe.objects.get(id=id)
            recipe_del.delete()
            return JsonResponse(
                {"message": "Recipe successfully deleted"},
                status=200
            )
        except Recipe.DoesNotExist:
            return JsonResponse(
            {"message": "Recipe does not exist, check again"},
            status=404
        )
    elif request.method == "PUT":
        recipe_content = json.loads(request.body)
        try:
            recipe_update = Recipe.objects.get(id=id)
            recipe_update.update(**recipe_content)
            return JsonResponse(
                recipe_content,
                encoder=RecipeEncoder,
                safe=False
            )
        except Recipe.DoesNotExist:
            return JsonResponse(
                {"message": "Recipe not found or invalid id"},
                status=404
            )
