from django.urls import path
from .views import recipe_detail, list_recipes

urlpatterns = [
    path("recipe/<int:id>/", recipe_detail, name="recipe_detial"),
]
